import BookingRequests from './requests/BookingRequests';

// TODO FRAMEWORK QUE UTILIZA DO JAVA SCRIPT, É ESCRITO A BASE DE FUNÇÕES!
// No contexto do Cypress, utilizamos as chaves describe e it para organizar e estruturar os casos de teste.
// O describe é uma suite onde podemos ter vários testes individuais dentro.

describe('Teste na camada de criação de Reservas', () => {

  beforeEach(() => {
    // cy.fixture() lê o arquivo JSON reservas.json e armazena os dados na variável data.
    cy.fixture('reservas.json').as('data');
    // a função .as é usada para atribuir um alias (apelido) à chamadas.
  });

  context('Criar reservas com Sucesso', () => {
    // As variáveis declaradas com let podem ser reatribuídas, ou seja, você pode atribuir um novo valor a elas.
    // As variáveis declaradas com const são de atribuição única e não podem ser reatribuídas após a sua inicialização.
    let bookingId;
  
    //  it define um teste individual, cada it representa um cenário ou um caso de teste específico.
    it('Deve criar reserva com depósito pago', () => {
      // cy.get para obter valor do alias e guardar em data
      cy.get('@data').then((data) => {
        const withDeposit = data.bookigWithDeposit;
  
        BookingRequests.createBooking(withDeposit).then((response) => {
          // then será executado quando a promessa for resolvida, ou seja, quando a requisição for concluída com sucesso.
          // guardando a resposta da requisição como argumento 'response'.
          expect(response.status).to.eq(200);
          expect(response.body).to.have.property('bookingid');
          expect(response.body.booking.depositpaid).to.eql(true);
          bookingId = response.body.bookingid;
        });
      });
    });
  
    it('Deve criar reserva sem depósito', () => {
      cy.get('@data').then((data) => {
        const withoutDeposit = data.bookigWithoutDeposit;
  
        BookingRequests.createBooking(withoutDeposit).then((response) => {
          expect(response.status).to.eq(200);
          expect(response.body).to.have.property('bookingid');
          expect(response.body.booking.depositpaid).to.eql(false);
          bookingId = response.body.bookingid;
        });
      });
    });

    afterEach(() => {
      BookingRequests.deleteBooking(bookingId).then((response) => {
        expect(response.status).to.eql(201)
      });
    });

  });

  context('Tentativa de criação de Reservas', () => {

    it('Deve dar erro ao tentar gerar reserva sem Nome', () => {
      cy.get('@data').then((data) => {
        const withoutName = data.bookingWithoutName;
  
        BookingRequests.createBooking(withoutName).then((response) => {
          expect(response.status).to.eq(500);
          expect(response.body).to.eql('Internal Server Error')
        });
      });
    });

  });

});