class BookingRequests {

  static createBooking(bookingData) {
    return cy.api({
      method: 'POST',
      url: '/booking',
      body: bookingData,
      // não queremos que o Cypress falhe automaticamente se a resposta for diferente de 2xx.
      failOnStatusCode: false,
    });
  }

  static deleteBooking(idBooking) {
    return cy.api({
      method: 'DELETE',
      // o acento grave, é utilizado quando precisamos concatenar uma string
      url: `/booking/${idBooking}`,
      headers: {
        Authorization: 'Basic YWRtaW46cGFzc3dvcmQxMjM='
      },
    });
  }

}

export default BookingRequests;